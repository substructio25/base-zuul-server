package com.difinite.zuul.config;

import com.difinite.zuul.model.DynamicRoute;
import com.difinite.zuul.repository.DynamicRouteRepo;
import com.difinite.zuul.service.ZuulDynamicRoutingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OnStart implements CommandLineRunner {
    @Autowired
    public DynamicRouteRepo dynamicRouteRepo;

    @Autowired
    ZuulDynamicRoutingService zuulDynamicRoutingService;

    @Override
    public void run(String... args) throws Exception {
        List<DynamicRoute> allRoutes = dynamicRouteRepo.findAll();
        for(DynamicRoute route : allRoutes){
            zuulDynamicRoutingService.addDynamicRoute(route);
        }
    }
}
