package com.difinite.zuul.repository;

import com.difinite.zuul.model.DynamicRoute;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DynamicRouteRepo extends JpaRepository<DynamicRoute, String> {

}
