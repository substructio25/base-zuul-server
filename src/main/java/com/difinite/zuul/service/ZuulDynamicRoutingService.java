package com.difinite.zuul.service;

import com.difinite.zuul.model.DynamicRoute;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.cloud.netflix.zuul.web.ZuulHandlerMapping;
import org.springframework.stereotype.Service;
import java.util.HashSet;

@Service
@Slf4j
public class ZuulDynamicRoutingService {
    private static final Logger logger = LoggerFactory.getLogger(ZuulDynamicRoutingService.class);
    private static final String HTTP_PROTOCOL = "http://";
    private final ZuulProperties zuulProperties;
    private final ZuulHandlerMapping zuulHandlerMapping;

    @Autowired
    public ZuulDynamicRoutingService(final ZuulProperties zuulProperties, final ZuulHandlerMapping zuulHandlerMapping){
        this.zuulProperties = zuulProperties;
        this.zuulHandlerMapping = zuulHandlerMapping;
    }

    public void addDynamicRoute(DynamicRoute dynamicRoute){
        StringBuilder sb = new StringBuilder((HTTP_PROTOCOL));
        if(dynamicRoute.getTargetURLPort() == 0){
            sb.append(dynamicRoute.getTargetURLHost());
            log.debug("Got it");
        }else{
            sb.append(dynamicRoute.getTargetURLHost()).append(":").append(dynamicRoute.getTargetURLPort());
        }

        if(StringUtils.isEmpty(dynamicRoute.getTargetURIPath())) {
            sb.append("");
        }else{
            sb.append(dynamicRoute.getTargetURIPath());
        }
        String url = sb.toString();

        zuulProperties.getRoutes().put(dynamicRoute.getRequestURIUniqueKey(), new ZuulProperties.ZuulRoute(dynamicRoute.requestURIUniqueKey, dynamicRoute.getRequestURI(), null, url, true, false, new HashSet<>()));

        zuulHandlerMapping.setDirty(true);
    }

    public Boolean removeDynamicRoute(final String requestURIUniqueKey){
        DynamicRoute dynamicRoute = new DynamicRoute();
        dynamicRoute.setRequestURIUniqueKey(requestURIUniqueKey);
        if(zuulProperties.getRoutes().containsKey(requestURIUniqueKey)){
            ZuulProperties.ZuulRoute zuulRoute = zuulProperties.getRoutes().remove(requestURIUniqueKey);
            zuulHandlerMapping.setDirty(true);
            return true;
        }
        return false;
    }
}
