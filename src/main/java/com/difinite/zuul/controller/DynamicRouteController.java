package com.difinite.zuul.controller;

import com.difinite.zuul.model.DynamicRoute;
import com.difinite.zuul.repository.DynamicRouteRepo;
import com.difinite.zuul.service.ZuulDynamicRoutingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DynamicRouteController {
    private static final Logger logger = LoggerFactory.getLogger(DynamicRouteController.class);

    @Autowired
    ZuulDynamicRoutingService zuulDynamicRoutingService;

    @Autowired
    DynamicRouteRepo dynamicRouteRepo;

    @RequestMapping(value="/api/load-all", method=RequestMethod.GET)
    public List loadAllSavedApi(){
        List<DynamicRoute> allRoutes = dynamicRouteRepo.findAll();
        return allRoutes;
    }

    @RequestMapping(value="/api/{key}")
    public DynamicRoute getDynamicRoute(@PathVariable("key") String key){
        return dynamicRouteRepo.findById(key).get();
    }

    @RequestMapping(value = "/api/add", method = RequestMethod.POST)
    public ResponseMessage getProxyURL(@RequestBody DynamicRoute dynamicRoute){
        zuulDynamicRoutingService.addDynamicRoute(dynamicRoute);
        dynamicRouteRepo.save(dynamicRoute);
        return new ResponseMessage("Route Added", "Your route successfully added");
    }

    @RequestMapping(value = "/api/delete/{key}", method = RequestMethod.POST)
    public ResponseMessage deleteProxyUrl(@PathVariable("key") String requestKey){
        Boolean response = zuulDynamicRoutingService.removeDynamicRoute(requestKey);
        dynamicRouteRepo.deleteById(requestKey);
        return new ResponseMessage("Route Removed", "Your route removed");
    }

    class ResponseMessage{
        public String status;
        public String message;
        public ResponseMessage(String status, String message){this.message = message; this.status = status;}
    }
}
